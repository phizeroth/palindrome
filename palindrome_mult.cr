

class Palindrome
  getter :palindromes

  ALPHABET = ('a'..'z').to_a
  
  def initialize(iter : UInt32, word_length : UInt8, threads : UInt8)
    @iter = iter
    @word_length = word_length
    @threads = threads
    @palindromes = Array(String).new
  end

  def generate
    ch = Channel(Nil).new

    @threads.times do
      spawn do
        puts "test "
        4194304.times do
          word = Array(Char).new(@word_length) { ALPHABET.sample(Random) }
          hit(word.join) if word === word.reverse
        end
        ch.send(nil)
      end
    end
    @threads.times { ch.receive }
  end

  private def hit(word : String)
    # @palindromes.push(word)
    puts word
  end
  
end


### MAIN ###
def main
  puts
  puts "========================"
  puts "   palindrome crystal   "
  puts "========================"
  puts "This program will generate 2^n random strings and return all palindromes."

  print "Enter power of two (2^_) [Default: #{default_n = 24_u8}]: "
  input_n = gets.not_nil!.chomp
  iter = 2_u32 ** (input_n == "" ? default_n : input_n.to_u8)

  print "String length: [Default: #{default_word_len = 8_u8}]: "
  input_word_len = gets.not_nil!.chomp
  word_len = input_word_len == "" ? default_word_len : input_word_len.to_u8

  print "Number of threads: [Default: #{default_threads = 4_u8}]: "
  input_threads = gets.not_nil!.chomp
  threads = input_threads == "" ? default_threads : input_threads.to_u8

  puts "-----------------------"

  p = Palindrome.new(iter, word_len, threads)
  elapsed = Time.measure { p.generate }
  
  puts "------- crystal -------"
  # puts "Out of #{iter.format} strings, found #{p.palindromes.size} palindromes in #{elapsed}."
  puts "Processed #{(iter/elapsed.total_seconds).to_i.format} strings per second."
  puts
end


### RUN ###
main