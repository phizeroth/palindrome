use std::io::{stdin, stdout, Write};
use std::time::Instant;
use std::thread;
use std::sync::Arc;
use std::sync::atomic::{AtomicU32, Ordering::Relaxed};
use rand::Rng;

fn reverse(string: &str) -> String {
    string.chars().rev().collect()
}

fn get_user_input(query: &str, default: u8) -> u8 {
    print!("{} [Default: {}]: ", query, default);
    stdout().flush().unwrap();

    let mut input = String::new();
    stdin().read_line(&mut input).expect("Failed to read line");

    if input == "\n" || input == "\r\n" {
        println!("{}", default);
        return default
    } else {
        return match input.trim().parse() {
            Ok(n) => n,
            Err(_) => {
                eprintln!("** Enter an integer **");
                get_user_input(query, default)
            }
        }
    }
}

fn generate(n: u32, word_len: u8, threads: u8) -> u32 {

    const ALPHABET: &[u8] = b"abcdefghijklmnopqrstuvwxyz";

    let count = Arc::new(AtomicU32::new(0));
    let mut children = vec![];

    for _ in 0..threads {
        let count = count.clone();
        children.push(thread::spawn(move || {

            let mut rng = rand::thread_rng();
            for _ in 0..(n / threads as u32) {
                let word: String = (0..word_len)
                    .map(|_| {
                        let idx = rng.gen_range(0, ALPHABET.len());
                        char::from(unsafe { *ALPHABET.get_unchecked(idx) })
                    })
                    .collect();

                if word == reverse(&word) {
                    count.fetch_add(1, Relaxed);
                    println!("{}", word);
                }
            }

        }));
    }

    for child in children {
        child.join().unwrap();
    }

    return count.load(Relaxed)
}

fn main() {

    println!("\n
=====================
   palindrome rust  
=====================\n
This multi-thread program will generate
2^n random strings and return all palindromes.\n"
    );
    
    let exp = get_user_input("Enter power of two (2^_)", 24);
    assert!(exp < 32, "INVALID POWER ENTRY: Must be >=0 and <32");
    let iter = 2u32.pow(exp.into());

    let word_len = get_user_input("Enter string length", 8);
    assert!(word_len > 0 && word_len <= 16, "INVALID STRING LENGTH: Must be >0 and <=16");

    let threads = get_user_input("Number of threads", 4);
    assert!(threads > 0 && threads <= 32, "INVALID THREAD ENTRY: Must be >0 and <=32");

    println!("------------------");

    let start_time = Instant::now();
    let count = generate(iter, word_len, threads);
    let duration = start_time.elapsed().as_millis() as u32;

    println!("------ rust ------");

    println!("Out of {} strings, found {} palindromes in {} seconds.",
        iter,
        count,
        duration as f64 / 1000 as f64
    );

    if duration > 0 {
        println!("Processed {} strings per second on {} thread{}.\n",
            (iter / duration) * 1000,
            threads,
            if threads == 1 {""} else {"s"}
        );
    }

}