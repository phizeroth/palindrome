require 'benchmark'

class Palindrome
  attr_reader :palindromes
  
  ALPHABET = ('a'..'z').to_a

  def initialize(iter, word_length)
    @iter = iter
    @word_length = word_length
    @palindromes = Array.new
  end

  def generate
    @iter.times do
      word = Array.new(@word_length) { ALPHABET.sample }
      hit(word.join) if word === word.reverse
    end
  end

  private def hit(word)
    @palindromes.push(word)
    puts word
  end

end


### MAIN ###
def main
  puts
  puts "====================="
  puts "   palindrome ruby   "
  puts "====================="
  puts "This program will generate 2^n random strings and return all palindromes."

  print "Enter power of two (2^_) [default: #{default_n = 24}]: "
  input_n = gets.chomp.to_i
  iter = 2 ** (input_n > 0 ? input_n : default_n)

  print "String length [default: #{default_word_len = 8}]: "
  input_word_len = gets.chomp.to_i
  word_len = input_word_len > 0 ? input_word_len : default_word_len

  puts "--------------------"

  p = Palindrome.new(iter, word_len)
  elapsed = Benchmark.measure { p.generate }

  puts "------- ruby -------"
  puts "Out of #{iter} strings, found #{p.palindromes.size} palindromes in #{elapsed.total.round(3)} seconds."
  puts "Processed #{(iter/elapsed.total).to_i} strings per second."
  puts
end


### RUN ###
main