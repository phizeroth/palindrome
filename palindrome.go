package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

const alphabet = "abcdefghijklmnopqrstuvwxyz"
var iter = int(math.Pow(2, 20))
const length = 8

func reverse(s string) string {
	rns := []rune(s)
	for i, j := 0, len(rns)-1; i < j; i, j = i+1, j-1 {
		rns[i], rns[j] = rns[j], rns[i]
	}
	return string(rns)
}

func randomWord() string {
	var seededRand *rand.Rand = rand.New(
		rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = alphabet[seededRand.Intn(len(alphabet))]
	}
	return string(b)
}

func main() {

	start := time.Now()

	var count = 0

	for i := 0; i < iter; i++ {

		word := randomWord()

		drow := reverse(word)
		if word == drow {
			fmt.Println(word)
			count++
		}
	}

	duration := time.Since(start)

	fmt.Printf("Out of %v strings, found %v palindromes in %v.\n", iter, count, duration)
}