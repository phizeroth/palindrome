import unicode, random, math, times

let startTime = getTime()
let alphabet = "abcdefghijklmnopqrstuvwxyz"
let iter = 2 ^ 21
let length = 8

randomize()

proc reverse*(str: string): string =
    str.reversed

proc randomWord(): string =
    for i in countup(1, length):
        result &= sample(alphabet)

var count = 0

for i in countup(1, iter):
    var word = randomWord()
    if word == word.reversed:
        echo word
        inc(count)

let elapsed = getTime() - startTime

echo "Out of ", iter, " strings, found ", count, " palindromes in ", elapsed