import random
import string
import multiprocessing
import time

start = time.time()

def gen(size=10, chars=string.ascii_uppercase):
    return ''.join(random.choice(chars) for _ in range(size))

count = []

def palindrome(iter, id):
    for _ in range(0, iter):
        random_string = gen(8)
        reversed_string = ''.join(reversed(random_string))
        if random_string == reversed_string:
            print(random_string)
#            count[id] += 1

if __name__ == "__main__":

    procs = 8
    iter = (2**22)
    jobs = []

    for i in range(0, procs):
        process = multiprocessing.Process(target=palindrome, args=(iter//procs, i))
        jobs.append(process)

    for j in jobs:
        j.start()

    for j in jobs:
        j.join()

    elapsed = time.time() - start

    print('Out of', iter, 'strings, found', count, 'in', elapsed, 'seconds.')
    print('Processed', iter/elapsed, 'strings per second on', procs, 'threads.')