class Palindrome {
    
    constructor (iter, wordLength) {
        this.iter = iter;
        this.wordLength = wordLength;
        this.alphabet = Array.from('abcdefghijklmnopqrstuvwxyz');
        this.palindromes = [];
    }

    generate () {
        for (let i=0; i < this.iter; i++) {
            let word = Array.from({length: this.wordLength}, () => this.alphabet[Math.floor(Math.random() * 25)]);
            let drow = word.reduce((ary, ele) => {
                ary.unshift(ele);
                return ary
            }, []);
            if (word.every((val, idx) => val === drow[idx])) {
                this.palindromes.push(word.join(''));
                console.log(word.join(''));
            }
        }
    }
}

p = new Palindrome(2**22, 8)

let startTime = new Date();
p.generate();
let elapsed = (new Date() - startTime) / 1000;

console.log(`Out of ${p.iter} strings, found ${p.palindromes.length} palindromes in ${elapsed} seconds.`);
console.log(`Processed ${p.iter/elapsed} strings per second.`);