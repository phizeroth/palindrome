#include <iostream>         // std:cout
#include <chrono>
#include <algorithm>        // std::reverse
#include <math.h>           // std::pow

using namespace std;

static const char alphabet[] = "abcdefghijklmnopqrstuvwxyz";
unsigned char stringLength = sizeof(alphabet) - 1;
unsigned int iter = pow(2,24);
unsigned short int tally;

char genRandom()
{
    return alphabet[rand() % stringLength];
}

int main()
{
    auto start = chrono::high_resolution_clock::now();

    cout << "-----------------" << endl;

    for(unsigned int j = 0; j < iter; j++) {
        string word;
        for(unsigned char i = 0; i < 8; ++i)
        {
            word += genRandom();
        }
        string drow = word;
        reverse(drow.begin(), drow.end());
        if(word == drow) {
            cout << word << endl;
            tally++;
        }
    }

    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);

    cout << "------ c++ ------" << endl;
    cout << "Out of " << iter << " strings, found " << tally << " palindromes in " << duration.count()/1000.0 << " seconds." << endl;
    cout << "Processed " << iter/duration.count()*1000 << " strings per second." << endl;
}