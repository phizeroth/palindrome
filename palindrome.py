import random
import string
import time
import array

start = time.time()
n = 8

def gen(size=10, chars=string.ascii_uppercase):
    return ''.join(random.choice(chars) for _ in range(size))

def palindrome(iter):
    count = 0

    for _ in range(0, iter):
        word = gen(n)
        drow = ''.join(reversed(word))
        if word == drow:
            print(word)
            count += 1
    return count

if __name__ == "__main__":

    iter = (2**22)
    count = palindrome(iter)

    elapsed = time.time() - start

    print('Out of', iter, 'strings, found', count, 'in', elapsed, 'seconds.')
    print('Processed', iter/elapsed, 'strings per second.')