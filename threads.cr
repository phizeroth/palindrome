ALPHABET = ('a'..'z').to_a

def generate
    (2**22).to_u32.times do
        word = Array(Char).new(8) { ALPHABET.sample(Random) }
        puts word.join if word === word.reverse
    end
end

ch = Channel(Nil).new

4.times do
    spawn do
        generate
        ch.send(nil)
    end
end

4.times { ch.receive }